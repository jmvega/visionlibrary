/*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#ifndef VISUALMEMORY_GABORFILTER_H
#define VISUALMEMORY_GABORFILTER_H

#include <stdio.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "fileOperations.h"
#include "../../mathOperations/cpp/mathOperations.h"

namespace visualMemory {
  class gaborFilter {
		public:
			gaborFilter ();
		  virtual ~gaborFilter ();

			CvMat** loadGaborFFT(char* folderName);
			void unloadGaborFFT(CvMat** mGabor);
			int extractGabor (IplImage* img,double* object,CvMat** mGabor);
			double* extractImageFeatures(const IplImage* img,int *nsize);
			int gaborKernel (int scale,int orientation,int mask_size,double kmax,double sigma,char* filename);
			int gaborFunction (char* base_fld);
			void extractFeatures (char *imagefile, char *imagedata);

		private:
			static const int IMAGE_SIZE;
			static const int DSCALE;
			static const int NUM_SCALES;
			static const int NUM_ORIENTATIONS;
			static const int DEBUG_GABOR;
	};
}

#endif
