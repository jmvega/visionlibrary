/*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#include <unistd.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdio.h>

#define DEBUG 0
#define SALIENCY_INCREMENT 1
#define SALIENCY_DECREMENT 1
#define MAX_SALIENCY 100
#define MIN_SALIENCY 0
#define LIVELINESS_INCREMENT 20
#define LIVELINESS_DECREMENT 10
#define MIN_LIVELINESS 0
#define MAX_LIVELINESS 5000
#define LIVELINESS_TO_DEAD -1
#define PENALTY_FACTOR 200
#define BONUS_FACTOR 100
#define FOLLOW_TIME 0.
#define TIME_TO_FORCED_SEARCH 4
#define TIME_TO_DEAD 50
#define TILT_MAX 90
#define TILT_MIN -90
#define CENTRO_X 260
#define CENTRO_Y 260
#define RADIO_MAX 258
#define RADIO_MIN 0
#define ANCHO_ESCENA_COMPUESTA 520
#define LONGITUDE_NUM_POSITIONS 3
#define MAX_FACES_IN_MEMORY 10
#define MIN_EXP_WIDTH 20
#define MIN_EXP_HEIGHT 20
#define MAX_GAP_FACES 20 // mm. máxima distancia entre caras que se consideran la misma
#define MAX_GAP_ARROWS 20
#define CACHE_SPACE 4000 // mm. de radio sobre el que están los objetos de cache
#define TIME_UPDATE_CACHE 15 // tiempo que transcurre entre cada actualización de cache
#define TIME_TO_SAVE_IMAGE 10 // tiempo que transcurre entre cada fotograma guardado de la memoria de líneas
#define TIME_MAINTENANCE 2

struct elementStruct {
	double lastInstant; // último instante de tiempo en su detección
	double firstInstant; // primer instante de tiempo en su detección
	float latitude; // posición absoluta del pantilt, en eje tilt
	float longitude; // posición absoluta del pantilt, en eje pan
	int scenePos; // it can be left, center or right, depends on pantilt pos where face was detected
	float saliency;
	float liveliness;
	int type; // 0 = elemento virtual; 1 = rectángulo; 2 = face; 3 = arrow
	int isVisited;
	struct elementStruct* next;
};

inline int getMaxSaliencyElement (struct elementStruct *myElements, struct elementStruct *myPrevMaxSaliency, struct elementStruct *myMaxSaliency, double actualInstant);
inline void updateElements (struct elementStruct *myElements, struct elementStruct *myMaxSaliency, double actualInstant);
