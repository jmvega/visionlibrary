/*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#ifndef VISUALMEMORY_SALIENCY_H
#define VISUALMEMORY_SALIENCY_H

#include <unistd.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdio.h>

struct elementStruct {
	double lastInstant; // último instante de tiempo en su detección
	double firstInstant; // primer instante de tiempo en su detección
	float latitude; // posición absoluta del pantilt, en eje tilt
	float longitude; // posición absoluta del pantilt, en eje pan
	int scenePos; // it can be left, center or right, depends on pantilt pos where face was detected
	float saliency;
	float liveliness;
	int type; // 0 = elemento virtual; 1 = rectángulo; 2 = face; 3 = arrow
	int isVisited;
	struct elementStruct* next;
};

namespace visualMemory {
  class saliency {
		public:
			saliency ();
		  virtual ~saliency ();

			int getMaxSaliencyElement (struct elementStruct *myElements, struct elementStruct *myPrevMaxSaliency, struct elementStruct *myMaxSaliency, double actualInstant);
			void updateElements (struct elementStruct *myElements, struct elementStruct *myMaxSaliency, double actualInstant);

		private:
			static const int DEBUG;
			static const int SALIENCY_INCREMENT;
			static const int SALIENCY_DECREMENT;
			static const int MAX_SALIENCY;
			static const int MIN_SALIENCY;
			static const int LIVELINESS_INCREMENT;
			static const int LIVELINESS_DECREMENT;
			static const int MIN_LIVELINESS;
			static const int MAX_LIVELINESS;
			static const int LIVELINESS_TO_DEAD;
			static const int PENALTY_FACTOR;
			static const int BONUS_FACTOR;
			static const float FOLLOW_TIME;
			static const int TIME_TO_FORCED_SEARCH;
			static const int TIME_TO_DEAD;
			static const int TILT_MAX;
			static const int TILT_MIN;
			static const int CENTRO_X;
			static const int CENTRO_Y;
			static const int RADIO_MAX;
			static const int RADIO_MIN;
			static const int ANCHO_ESCENA_COMPUESTA;
			static const int LONGITUDE_NUM_POSITIONS;
			static const int MAX_FACES_IN_MEMORY;
			static const int MIN_EXP_WIDTH;
			static const int MIN_EXP_HEIGHT;
			static const int MAX_GAP_FACES; // mm. máxima distancia entre caras que se consideran la misma
			static const int MAX_GAP_ARROWS;
			static const int CACHE_SPACE; // mm. de radio sobre el que están los objetos de cache
			static const int TIME_UPDATE_CACHE; // tiempo que transcurre entre cada actualización de cache
			static const int TIME_TO_SAVE_IMAGE; // tiempo que transcurre entre cada fotograma guardado de la memoria de líneas
			static const int TIME_MAINTENANCE;
	};
}

#endif
