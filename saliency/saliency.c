/*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#include "saliency.h"
/*
inline int elementAttainable (struct elementStruct *element, HPoint3D cameraPosition) {
	HPoint3D p;
	float pan, tilt;
	float isAttainable = FALSE;

	p.X = p.X - robotCamera.position.X;
	p.Y = p.Y - robotCamera.position.Y;
	p.Z = 0. - robotCamera.position.Z;
	p.H = 1.;

	if (p.X != 0.) {
		pan = atan(p.Y/p.X);
		tilt = atan (p.Z/p.X); // La Z siempre saldrá negativa, porque estará más alta que el punto

		pan = pan * 180.0/PI; // pasamos a grados
		tilt = tilt * 180.0/PI;

		if ((pan >= -90) && (pan <= 90) && (tilt < -20) && (tilt > -50))
			isAttainable = TRUE;
	}

	return (isAttainable);
}
*/

inline int getMaxSaliencyElement (struct elementStruct *myElements, struct elementStruct *myPrevMaxSaliency, struct elementStruct *myMaxSaliency, double actualInstant) {
	struct elementStruct *p, *q, *r, *prev;
	float maxSaliency = -9999.;
	p = myElements;
	q = NULL;
	r = p;

	// Recorremos la lista buscando al de mayor saliencia
	while (p != NULL) {
		if ((p->saliency > maxSaliency)) { // && (elementAttainable (p))) { // elemento atendible
			maxSaliency = p->saliency;
			q = p; // me lo guardo
			prev = r; // r tendrá el anterior al maxSaliency
		}
		r = p;
		p = p->next; // preparamos a p para la siguiente vuelta :)
	}

	if (q != NULL) { // nos aseguramos que q tenga algo, para que no casque
		myMaxSaliency = q; // q tendrá al elemento con mayor saliencia

		if (myMaxSaliency == myElements) // como es el primero, el previo al maxSaliency es Null
			myPrevMaxSaliency = NULL;
		else
			myPrevMaxSaliency = prev;

		myMaxSaliency->saliency = MIN_SALIENCY;
		myMaxSaliency->liveliness = MAX_LIVELINESS;
		myMaxSaliency->lastInstant = actualInstant;
		return (1);
	} else {
		if (DEBUG) printf ("getMaxSaliency: Fatal error - Max Saliency element NOT FOUND!\n");
		myMaxSaliency = NULL; // no tenemos candidato ahora mismo
		myPrevMaxSaliency = NULL;
		return (-1);
	}
}

inline void updateElements (struct elementStruct *myElements, struct elementStruct *myMaxSaliency, double actualInstant) {
	struct elementStruct *p, *q, *virtualMax, *virtualPrevMax;
	virtualPrevMax = NULL;
	virtualMax = NULL;

	p = myElements;
	q = p;

	// 1º - Recorremos la lista eliminando todos los que se les ha acabado la vida :(
	while (p != NULL) {
		if (p == myMaxSaliency) {
			virtualPrevMax = q;
			virtualMax = p;
		}

		if ((actualInstant - p->lastInstant) > TIME_TO_DEAD) { // esta antigua elemento será borrada de memoria
			if (DEBUG) printf ("updateElements: Deleting old element of memory...\n");
			if (p == myElements) {
				myElements = myElements->next;
				free (p);
				p = NULL;
				p = myElements;
			} else {
				q->next = p->next;
				free (p);
				p = NULL;
				p = q->next;
			}
		} else {
			p->liveliness -= LIVELINESS_DECREMENT;
			p->saliency += SALIENCY_INCREMENT;

			q = p;
			p = p->next; // preparamos a p para la siguiente vuelta :)
		}
	}
}

