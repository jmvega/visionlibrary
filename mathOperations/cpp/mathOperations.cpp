 /*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#include "mathOperations.h"

namespace visualMemory {

	mathOperations::mathOperations () {}

	static double meanVector (double* v, int vSize) {
		int i;   
		double mean=0.0;
		double* ptr = v;

		for (i=0;i<vSize;i++) {
			mean+= *ptr;
			ptr++;
		}
		mean /=(double)vSize;
		return mean;
	}

	static void meanUnitVector (double* v, int vSize) {
		double sqsum = 0.0;
		double mean = meanVector(v,vSize);
		double* ptr = v;
		int i;

		for(i=0;i<vSize;i++) {
			(*ptr) -= mean; 
			sqsum += (*ptr)*(*ptr);
			ptr++;
		}

		double a = 1.0f/(double)(sqrt(sqsum));
		ptr = v;

		for(i=0;i<vSize;i++) {
			(*ptr) *= a;
			ptr++;
		}    
	}

	static int multiplyFFT (const CvArr* srcAarr, const CvArr* srcBarr, CvArr* dstarr) {
		CvMat *srcA = (CvMat*)srcAarr;
		CvMat *srcB = (CvMat*)srcBarr;
		CvMat *dst = (CvMat*)dstarr;

		int i,j, rows, cols;
		rows = srcA->rows;
		cols = srcA->cols;
		double c_re,c_im;

		for( i=0; i<rows; i++ )	{
			for( j = 0; j < cols; j ++ ) {
				c_re = ((double*)(srcA->data.ptr + srcA->step*i))[j*2]*((double*)(srcB->data.ptr + srcB->step*i))[j*2] -
				((double*)(srcA->data.ptr + srcA->step*i))[j*2+1]*((double*)(srcB->data.ptr + srcB->step*i))[j*2+1];
				c_im = ((double*)(srcA->data.ptr + srcA->step*i))[j*2]*((double*)(srcB->data.ptr + srcB->step*i))[j*2+1] +
				((double*)(srcA->data.ptr + srcA->step*i))[j*2+1]*((double*)(srcB->data.ptr + srcB->step*i))[j*2];
				((double*)(dst->data.ptr + dst->step*i))[j*2]	= c_re;
				((double*)(dst->data.ptr + dst->step*i))[j*2+1]	= c_im;
			}
		}

		return 1;
	}

	mathOperations::~mathOperations () {}
}

int main () {}
