/*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#include "fileOperations.h"

namespace visualMemory {

	fileOperations::fileOperations () {}

	static int fileOperations::saveMatrixImage (CvMat* mat, char* imageName) {
		double maxVal,minVal;
		CvPoint minLoc, maxLoc;
		cvMinMaxLoc( mat, &minVal, &maxVal,&minLoc, &maxLoc, 0 );
		CvSize imsize={mat->rows,mat->cols};
		IplImage* tt=cvCreateImage(imsize,IPL_DEPTH_32F,0);	
		tt= cvGetImage(mat,  tt);
		IplImage* ipl=cvCreateImage(imsize ,8,1);
		cvConvertScale(tt,ipl,255.0/(maxVal-minVal),-minVal*255.0/(maxVal-minVal));	
		cvSaveImage(imageName,ipl);
		cvReleaseImage(&ipl);
		return 1;
	}

	static CvMat* fileOperations::getMatrix (FILE *fh, int isVector) {
		FILE *Stream;				//Stream for output.
		int cIndex=0;				//Counter for elements in the matrix.
		double Dim[2];				//The dimensions of the loaded matrix.
		double* Elem;				//Pointer to the elements the matrix.

		if (fh==NULL) exit (-1);
		Stream = fh;

		if (fread((void*)Dim,sizeof(double),2,Stream) <2) { //Retrive the dimensions.
			printf("Can't allocate memory for saving the matrix!");
			return(0);
		}

		int rows,cols;
		if(!isVector)	{
			rows = (int)Dim[0];//Save the dimensions of the matrix.
			cols = (int)Dim[1];
		} else	{
			cols = (int)Dim[0];//Save the dimensions of the matrix.
			rows = (int)Dim[1];
		}

		Elem=(double*)malloc(rows*cols*sizeof(double));	//Allocate memory to retrive the data.
		if(Elem==NULL) printf("Out of memory.");

		if (fread((void*)Elem,sizeof(double),rows*cols,Stream) < (unsigned)(rows*cols)) { //Retrive the data.
			printf("Problem reading from file.");
		}

		CvMat* m  = cvCreateMatHeader(rows,cols,CV_64FC1);
		cvCreateData (m);
		int cI,cJ;
		for(cI=0;cI<rows;cI++) { // Put the elements into the matrix.
			for(cJ=0;cJ<cols;cJ++) {
				CV_MAT_ELEM( *m,double,cI,cJ)=Elem[cIndex];
				cIndex++;
			}
		}
		free(Elem);	
		return m; // Free allocated memory.
	}

	static int fileOperations::writeMatrix (CvMat* m, FILE *fh, int isVector) {
		FILE *Stream;					//Stream for output.
		int cIndex=2;					//Counter for elements in the matrix.
		int cSize=m->rows *m->cols+2;	//The number of elements to save.
		double*Elem;					//Pointer to all the element to save.

		assert( fh!=NULL );
		Stream = fh;

		Elem=(double*)malloc(cSize*sizeof(double)); //Allocate memory to Elem.
		if(Elem==NULL) {
			printf("Can't allocate memory for saving the matrix!");
			return(0);
		}

		if(!isVector) {
			Elem[0]=(double)m->rows;//Save the dimensions of the matrix.
			Elem[1]=(double)m->cols;
		} else	{
			Elem[0]=1.0;//Save the dimensions of the matrix.
			Elem[1]=(double)m->rows;
		}
		int cI,cJ;
		for(cI=0;cI<m->rows ;cI++) { //Write the elements of the matrix to Elem.
			for(cJ=0;cJ<m->cols;cJ++)	{
				Elem[cIndex]=CV_MAT_ELEM( *m,double,cI,cJ);
				cIndex++;
			}
		}

		if(fwrite((void*)Elem,sizeof(double),cSize,Stream) < (unsigned)cSize) { // Save the data.
			printf("Error, can't save the matrix!");
			return(0);
		}

		free(Elem);	// Free memory.
		return(1);
	}

	static int fileOperations::writeData(double* v, int length, FILE* fh) {
		FILE *Stream;					//Stream for output.
		double*Elem;					//Pointer to all the element to save.

		assert( fh!=NULL );
		Stream = fh;

		Elem=(double*)malloc((length+2)*sizeof(double));//Allocate memory to Elem.
		if(Elem==NULL) {
			printf("Can't allocate memory for saving the matrix!");
			return(0);
		}

		Elem[0]=(double)1;//Save the dimensions of the matrix.
		Elem[1]=(double)length;

		int i;
		for(i=0;i<length ;i++) Elem[i+2]=v[i];
	
		if(fwrite((void*)Elem,sizeof(double),(length+2),Stream) < (unsigned)(length+2)) { // Save the data.
			printf("Error, can't save the matrix!");
			return(0);
		}
		free(Elem);	
		return(1);
	}

	static double* fileOperations::getData (FILE* fh, int &length) {
		FILE *Stream;					// Stream for output.
		double*Elem;					// Pointer to all the element to save.
		double Dim[2];				// The dimensions of the loaded matrix.

		assert( fh!=NULL );
		Stream = fh;

		if(fread((void*)Dim,sizeof(double),2,Stream) <2 ) { // Retrive the dimensions.
			printf("Can't allocate memory for saving the matrix!");
			return(0);
		}
	 
		length=(int)Dim[1];

		Elem=(double*)malloc(length*sizeof(double)); // Allocate memory to Elem.
		if(Elem==NULL) {
			printf("Can't allocate memory for saving the matrix!");
			return(0);
		}
	
		if(fread((void*)Elem,sizeof(double),length,Stream) < (unsigned)length) { // Save the data.
			printf("Error, can't save the matrix!");
			return(0);
		}

		return Elem;
	}
	fileOperations::~fileOperations() {	}
}

//int main () {}

